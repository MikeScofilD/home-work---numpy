import numpy

# Задание 1
# 1)Сохраните в файл данные об изменениях температуры в течение трёх месяцев
# (сгенерируйте случайные значения- 90 элементов)
# и, используя средства Numpy, загрузите эти данные из файла в ndarray
# (плоский список, значения расположены по два в одной строке).
weather = numpy.random.random_integers(15, 40, 90)
print(weather[0].dtype)
# print(weather)
print(type(weather))
# file = 'weather.txt'
# with open(file, 'w') as f:
#    str_weather = weather
#    f.write(' '.join(map(str.encode, str_weather.)) + '\n')
numpy.savetxt("weather.csv", weather, delimiter=",")

for c in weather:
    print(f"°C:{c}")
# 2)Преобразуйте получившийся набор данных в плоский массив ndarray.

# 3)Выведите максимальное, минимальное, среднее значения температур среди существующих значений.
# Для решения данной задачи используйте оптимальный тип данных Numpy,
max_w = weather.max()
min_w = weather.min()
Secondary = int(weather.sum() / weather.__len__())
print(f"Max °C: {max_w}")
print(f"Min °C: {min_w}")
print(f"Secondary °C: {Secondary}")

# учитывая, что замеры производились в летнее время и температура не падает ниже +15 С.

# 4)Разбейте общую последовательность на три равных,
# чтобы получился двумерный массив 3 x 30 и сохраните последние 5 значений каждого месяца в
# текстовый файл (массив размерности 3 x 5).
print()
print(weather)
weather.shape = (3, 30)
print(weather)